# Libket Dev Container

This container ensured binary-compatible with applications that must interoperate with Libket on Linux.

For Visual Studio Code Dev Containers workspaces, you can use the file `.devcontainer/devcontainer.json` below as a basis.

```json
{
	"name": "Ket",
	"image": "registry.gitlab.com/quantum-ket/devcontainer",
	"extensions": [
		"rust-lang.rust-analyzer",
		"ms-vscode.cpptools-extension-pack",
		"ms-python.python"
	],
	"workspaceMount": "",
	"runArgs": [
		"--volume=${localWorkspaceFolder}:/workspaces/${localWorkspaceFolderBasename}:Z",
	],
	"containerUser": "quantum"
}
```

# Tags

* `registry.gitlab.com/quantum-ket/devcontainer:0.5` For Ket 0.5.x
  